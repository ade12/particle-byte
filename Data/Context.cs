﻿using Microsoft.EntityFrameworkCore;
using WebApplication4.Models;

namespace WebApplication4.Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().ToTable("Company");
            modelBuilder.Entity<Employee>().ToTable("Employee");
            modelBuilder.Entity<User>().ToTable("User");

            modelBuilder.Entity<User>()
            .HasData(
                new User
                {
                    Email = "admin@grtech.com.my",
                    Password = "password"
                },
                new User
                {
                    Email = "user@grtech.com.my",
                    Password = "password"
                }
            );
        }
    }
}
