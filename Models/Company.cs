﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Models
{
    public class Company
    {
        public string Name { get; set; }
        [Key]
        public string Email { get; set; }
        public string Logo { get; set; }
        public string Website { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
