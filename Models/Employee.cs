﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApplication4.Data;

namespace WebApplication4.Models
{
    public class Employee
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Key]
        [Required]
        public string Email { get; set; }
        public string Phone { get; set; }
        [Required]
        public string CompanyEmail { get; set; }

        public virtual Company Company { get; set; }
    }
}
